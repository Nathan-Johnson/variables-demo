﻿using System;

namespace variables_demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("***************************");
            Console.WriteLine("***  Welcome to my app  ***");
            Console.WriteLine("***************************");
            Console.WriteLine("                           ");
            Console.WriteLine("===========================");
            Console.WriteLine("Please type in a number");

            var num1 = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed in multiplied by one is {num1 * 1}");
            Console.WriteLine($"The number you typed in multiplied by two is {num1 * 2}");
            Console.WriteLine($"The number you typed in multiplied by three is {num1 * 3}");
            Console.WriteLine($"The number you typed in multiplied by four is {num1 * 4}");
            Console.WriteLine($"The number you typed in multiplied by five is {num1 * 5}");
            Console.WriteLine($"The number you typed in multiplied by six is {num1 * 6}");
            Console.WriteLine($"The number you typed in multiplied by seven is {num1 * 7}");
            Console.WriteLine($"The number you typed in multiplied by eight is {num1 * 8}");
            Console.WriteLine($"The number you typed in multiplied by nine is {num1 * 9}");
            Console.WriteLine($"The number you typed in multiplied by ten is {num1 * 10}");
            Console.WriteLine($"The number you typed in multiplied by eleven is {num1 * 11}");
            Console.WriteLine($"The number you typed in multiplied by twelve is {num1 * 12}");
            Console.WriteLine($"The number you typed in multiplied by one thousand seven hundred and fifty two is {num1 * 1752}");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
